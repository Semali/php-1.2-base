<?php
$compNumber = 73;
$userNumber = $_GET['number'];
function guessNumber($userNumber, $compNumber) {
	if ($userNumber == null) return 'Введите число';
	if ($userNumber > $compNumber) {
		return 'Много';
	} elseif ($userNumber < $compNumber) {
		return 'Мало';
	} else {
		return 'Вы угадали';
	}
}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>Netology. Homework.</title>
	</head>
	<body>
		<h1>Угадай число!</h1>
		<h2>Загадо целое число от 0 до 100, сумеешь ли отгадать?</h2>
		<form id="form" method="GET">
			Введите число: <input type="text" name="number"><br>
			<input type="submit" value="Проверить">
		</form>
		<h3>Результат:</h3>
		<textarea name="result" cols="30" rows="2"><?= guessNumber($userNumber, $compNumber) ?></textarea>
	</body>
</html>